import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HTTP } from '@ionic-native/http/ngx';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-type':'application/x-www-form-urlencoded' })
}; 
 
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  readonly rootUrl = 'http://192.168.1.28:8000/';


  constructor(private http:HttpClient, private httpp:HTTP) { }

  login(userPhone, password){
    var data = "phone="+userPhone+"&password="+password;
    var reqHeader = new HttpHeaders({'Content-type':'application/x-www-form-urlencoded', 'No-Auth':'True'});
    return this.httpp.post(this.rootUrl+'/api/loginuser/', data,{headers: reqHeader})
  }

  createUser(user){
    const url = 'http://192.168.1.38:8000/api/registeruser/'
    return this.httpp.post(url, user, {});
  }
 

 
 
 
  logout() {
    localStorage.removeItem('userToken');
  }
  
}
