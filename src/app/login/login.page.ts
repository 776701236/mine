import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  isLoginError : boolean = false;

  constructor(private authService:AuthenticationService, private router:Router, private http:HttpClient) { }

  ngOnInit() {
  }

  onLogin(UserPhone, UserPassword) {
    this.authService.login(UserPhone, UserPassword).then((data : any) =>{
      localStorage.setItem('userToken',data.token);
      console.log(data);
      if(data.data){
        this.router.navigateByUrl('/home');
        console.log(data.data, 'in the house')
      }else{
        this.router.navigateByUrl('login');
        console.log('mot de passe incorrect') 
      }
    }, 
    (err : HttpErrorResponse)=>{
      this.isLoginError = true;
    });
  }

}
