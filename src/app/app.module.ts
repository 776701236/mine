import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { HTTP } from '@ionic-native/http/ngx';
import { AuthenticationService } from './services/authentication.service';
 

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, NgbModule.forRoot(),
    IonicModule.forRoot(),
    IonicStorageModule.forRoot({ 
      name: 'MyLoc',
driverOrder: ['indexeddb', 'sqlite', 'websql']
    })
    , AppRoutingModule, HttpClientModule],
  providers: [ AuthenticationService,
    StatusBar,
    SplashScreen, 
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }, HTTP
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
