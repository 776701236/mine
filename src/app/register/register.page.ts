import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  constructor(private authService:AuthenticationService, private route:Router) { }

  ngOnInit() {
  }

  onRegister(user) {
    console.log(user);
    this.authService.createUser(user)
    .then(data =>{
      console.log(data.data)
      this.route.navigateByUrl('/home');
  
   }, err => {
      console.log(err);
   });
  }

}
